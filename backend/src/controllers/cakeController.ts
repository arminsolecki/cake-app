import { Router, Request, Response } from 'express';
import { CakesRepository } from '../repository/CakesRepository';

const router = Router();

router.get('/', async (req: Request, res: Response) => {
  try {
    const cakes = await CakesRepository.getAll();
    res.json(cakes);
  } catch (error) {
    res.status(500).json({ success: false });
  }
});

router.get('/:id', async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const cake = await CakesRepository.getById(id);
    if (cake) {
      res.json(cake);
    } else {
      res.status(404).json({ success: false });
    }
  } catch (error) {
    res.status(500).json({ success: false });
  }
});

router.post('/', async (req: Request, res: Response) => {
  const { name, comment, imageUrl, yumFactor } = req.body;

  if (name && comment && imageUrl && yumFactor) {
    try {
      await CakesRepository.createCake({ name, comment, imageUrl, yumFactor });
      res.json({ success: true });
    } catch (error) {
      res.status(500).json({ success: false });
    }
  } else {
    res.status(400).json({ success: false });
  }
});

router.put('/:id', async (req: Request, res: Response) => {
  let { id } = req.params;
  const { name, comment, imageUrl, yumFactor } = req.body;

  if (name && comment && imageUrl && yumFactor) {
    try {
      await CakesRepository.updateCake(id, { name, comment, imageUrl, yumFactor });
      res.json({ success: true });
    } catch (error) {
      res.status(500).json({ success: false });
    }
  } else {
    res.status(400).json({ success: false });
  }
});

router.delete('/:id', async (req: Request, res: Response) => {
  let { id } = req.params;
  try {
    await CakesRepository.deleteCake(id);
    res.json({ success: true });
  } catch (error) {
    res.status(500).json({ success: false });
  }
});

export const CakeController = router;
