import { Table, Column, Model, AutoIncrement, PrimaryKey, CreatedAt, UpdatedAt } from 'sequelize-typescript';

export interface CakeModel {
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: number;
}

export interface CakeDbModel extends CakeModel {
  id: number;
}

@Table
export class Cake extends Model<Cake> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Column
  comment: string;

  @Column
  imageUrl: string;

  @Column
  yumFactor: number;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
