import { Sequelize } from 'sequelize-typescript';
import { Cake } from './models/cake';
import { CakesRepository } from '../repository/CakesRepository';

const env = process.env.NODE_ENV || 'development';
const config = require('../../config/config.json')[env];

const seed = [
  {
    name: 'Cake 1',
    comment: 'Yummy 1',
    imageUrl: 'cake1.jpeg',
    yumFactor: 1
  },
  {
    name: 'Cake 2',
    comment: 'Yummy 2',
    imageUrl: 'cake2.jpeg',
    yumFactor: 2
  },
  {
    name: 'Cake 3',
    comment: 'Yummy 3',
    imageUrl: 'cake3.jpeg',
    yumFactor: 3
  },
  {
    name: 'Cake 4',
    comment: 'Yummy 4',
    imageUrl: 'cake4.jpeg',
    yumFactor: 4
  },
  {
    name: 'Cake 5',
    comment: 'Yummy 5',
    imageUrl: 'cake5.jpeg',
    yumFactor: 5
  }
];

class Database {
  sequelize: Sequelize;
  async init() {
    this.sequelize = new Sequelize(config);

    this.sequelize.addModels([Cake]);

    await this.sequelize.sync();
  }

  async seed() {
    seed.forEach(cake => {
      CakesRepository.createCake(cake);
    });
  }
}

export const database = new Database();
