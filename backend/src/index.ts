import { app } from './app';
import { database } from './database/database';

const port = process.env.PORT || 4000;

app.listen(port, async () => {
  await database.init();

  await database.seed();

  console.info(`Server running on port ${port}`);
});
