import { Cake, CakeModel } from '../database/models/cake';

export class CakesRepository {
  static async getAll() {
    const allCakes = await Cake.all();

    return allCakes;
  }

  static async getById(id: number) {
    const cake = await Cake.findById(id);

    return cake;
  }

  static async createCake(cake: CakeModel) {
    await Cake.create(cake);

    return true;
  }

  static async updateCake(id: number, newCake: CakeModel) {
    const cake = await Cake.findById(id);
    if (cake) {
      await cake.updateAttributes(newCake);

      return true;
    }

    return false;
  }

  static async deleteCake(id: number) {
    await Cake.destroy({
      where: {
        id: id
      }
    });

    return true;
  }
}
