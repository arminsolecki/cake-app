import { app } from './app';
import { createCakeModel } from './__test-utils__';

var request = require('supertest');

jest.mock('./repository/CakesRepository', () => {
  const createCake = require('./__test-utils__').createCakeModel;
  const cakeList = [0, 1, 2, 3].map(createCake);
  return {
    CakesRepository: {
      getAll: () => cakeList,
      getById: (id: number) => cakeList[id],
      createCake: () => true
    }
  };
});

describe('app', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should get all cakes', async done => {
    const expected = [0, 1, 2, 3].map(createCakeModel);
    request(app)
      .get('/api/cake/')
      .expect('Content-Type', /json/)
      .expect(200, expected, done);
  });

  it('should return single cake by id', async done => {
    const expected = createCakeModel(3);
    request(app)
      .get('/api/cake/3')
      .expect('Content-Type', /json/)
      .expect(200, expected, done);
  });

  it('should create a cake', async done => {
    const newCake = createCakeModel(3);
    request(app)
      .post('/api/cake')
      .send(newCake)
      .expect('Content-Type', /json/)
      .expect(200, { success: true }, done);
  });
});
