import express from 'express';
import { CakeController } from './controllers/cakeController';
import { json, urlencoded } from 'body-parser';

const apiPath = '/api';

const app: express.Application = express();

app.use(json());
app.use(urlencoded({ extended: true }));

app.use(`${apiPath}/cake`, CakeController);

export { app };
