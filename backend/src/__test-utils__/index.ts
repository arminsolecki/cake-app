import { CakeDbModel } from '../database/models/cake';

export function createCakeModel(x: number): CakeDbModel {
  return {
    id: x,
    name: `Name ${x}`,
    comment: 'Comment',
    imageUrl: 'Image Url',
    yumFactor: (x % 5) + 1
  };
}
