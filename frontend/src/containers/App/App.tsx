import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Routes } from '../../constants/Routes';
import { AddCakeView } from '../AddCakeView/AddCakeView';
import { CakeDetailsView } from '../CakeDetailsView/CakeDetailsView';
import { CakesView } from '../CakesView/CakesView';
import './App.css';

class App extends React.Component {
  public render() {
    return (
      <div className="app">
        <header className="app__header">
          <h1 className="app__title">Cake app</h1>
        </header>
        <div className="app__container">
          <Switch>
            <Route exact={true} path={Routes.Home} component={CakesView} />
            <Route path={Routes.AddCake} component={AddCakeView} />
            <Route path={Routes.ViewCake} component={CakeDetailsView} />
          </Switch>
        </div>
      </div>
    );
  }
}

export { App };
