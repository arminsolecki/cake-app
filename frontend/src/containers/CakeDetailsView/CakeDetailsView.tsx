import * as React from 'react';
import { FullCake } from '../../components/FullCake/FullCake';
import { Routes } from '../../constants/Routes';
import { Link, withRouter } from 'react-router-dom';

import { MapDispatchToPropsFunction, MapStateToProps, connect } from 'react-redux';
import { getFullCake } from '../../actions/api';
import selectors from '../../selectors';
import { CakeProps } from '../../components/Cake/Cake';
import { RouteComponentProps } from 'react-router';

interface CakeDetailsViewProps {
  cake?: CakeProps;
}

interface CakeDetailsViewDispatchProps {
  getCake: (id: number) => any;
}

class CakeDetailsViewComponent extends React.Component<
  CakeDetailsViewProps & CakeDetailsViewDispatchProps & RouteComponentProps<any>,
  any
> {
  async componentDidMount() {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    this.props.getCake(id);
  }
  public render() {
    const { cake } = this.props;
    return (
      <div>
        <h2>Cake Details</h2>

        {cake ? <FullCake {...cake} /> : 'Cake not available'}

        <Link to={Routes.Home}>Back to all cakes</Link>
      </div>
    );
  }
}

export { CakeDetailsViewComponent };

export const mapStateToProps: MapStateToProps<CakeDetailsViewProps, any, any> = (state, ownProps) => {
  return {
    cake: selectors.cake.getFullCake(state)
  };
};

export const mapDispatchToProps: MapDispatchToPropsFunction<CakeDetailsViewDispatchProps, any> = dispatch => {
  return {
    getCake: (id: number) => dispatch(getFullCake(id) as any)
  };
};

export const CakeDetailsView = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CakeDetailsViewComponent)
);
