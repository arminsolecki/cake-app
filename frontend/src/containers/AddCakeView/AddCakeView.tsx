import * as React from 'react';
import { CakeForm } from '../../components/CakeForm/CakeForm';
import { Link, withRouter } from 'react-router-dom';
import { Routes } from '../../constants/Routes';
import { MapDispatchToPropsFunction, connect } from 'react-redux';
import { createCake } from '../../actions/api';

interface AddCakeViewDispatchProps {
  addCake: (name: string, comment: string, imageUrl: string, yumFactor: string) => any;
}

class AddCakeViewComponent extends React.Component<AddCakeViewDispatchProps, any> {
  constructor(props: any) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(name: string, comment: string, imageUrl: string, yumFactor: string) {
    this.props.addCake(name, comment, imageUrl, yumFactor);
  }

  public render() {
    return (
      <div>
        <h2>Add new cake</h2>

        <CakeForm onSubmit={this.handleSubmit} />

        <Link to={Routes.Home}>Back to all cakes</Link>
      </div>
    );
  }
}

export { AddCakeViewComponent };

export const mapDispatchToProps: MapDispatchToPropsFunction<AddCakeViewDispatchProps, any> = dispatch => {
  return {
    addCake: (name, comment, imageUrl, yumFactor) => dispatch(createCake(name, comment, imageUrl, yumFactor) as any)
  };
};

export const AddCakeView = withRouter(
  connect(
    null,
    mapDispatchToProps
  )(AddCakeViewComponent)
);
