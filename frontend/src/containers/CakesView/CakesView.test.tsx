import * as React from 'react';
import { mount } from 'enzyme';
import { CakeProps } from '../../components/Cake/Cake';
import { MemoryRouter } from 'react-router-dom';
import { CakesViewComponent } from './CakesView';
import { createCake } from '../../__test-utils__';

describe('<CakesView />', () => {
  const loadCakes = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render cakes', async () => {
    // Arrange
    const cakes: CakeProps[] = [1, 2, 3, 4].map(createCake);

    const element = (
      <MemoryRouter>
        <CakesViewComponent cakes={cakes} loadCakes={loadCakes} />
      </MemoryRouter>
    );

    // Act
    const wrapper = await mount(element);

    wrapper.update();
    const renderedCakes = wrapper.find('[data-selector="cake"]');
    // Assert
    expect(renderedCakes).toHaveLength(4);
    cakes.forEach((cake, index) => {
      expect(renderedCakes.at(index).text()).toBe(cake.name);
    });
    expect(loadCakes).toBeCalled();
  });

  it('should render error message when there are no cakes', () => {
    // Arrange
    const element = (
      <MemoryRouter>
        <CakesViewComponent cakes={[]} loadCakes={loadCakes} />
      </MemoryRouter>
    );

    // Act
    const wrapper = mount(element);

    // Assert
    expect(wrapper.find('[data-selector="cake-list"]').text()).toBe('No cakes available');
    expect(loadCakes).toBeCalled();
  });
});
