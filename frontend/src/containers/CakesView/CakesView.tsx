import * as React from 'react';
import { CakeList } from '../../components/CakeList/CakeList';
import { Routes } from '../../constants/Routes';
import { Link, withRouter } from 'react-router-dom';

import { CakeProps } from '../../components/Cake/Cake';
import { MapDispatchToPropsFunction, MapStateToProps, connect } from 'react-redux';
import { getAllCakes } from '../../actions/api';
import selectors from '../../selectors';

interface CakesViewProps {
  cakes: CakeProps[];
}

interface CakesViewDispatchProps {
  loadCakes: () => any;
}

class CakesViewComponent extends React.Component<CakesViewProps & CakesViewDispatchProps, any> {
  constructor(props: any) {
    super(props);
  }

  async componentDidMount() {
    this.props.loadCakes();
  }

  public render() {
    return (
      <div>
        <Link to={Routes.AddCake}>Add new cake</Link>
        <h2>List of cakes</h2>
        <div data-selector="cake-list">
          <CakeList cakes={this.props.cakes} />
        </div>
      </div>
    );
  }
}

export { CakesViewComponent };

export const mapStateToProps: MapStateToProps<CakesViewProps, any, any> = (state, ownProps) => {
  return {
    cakes: selectors.cake.getAllCakes(state)
  };
};

export const mapDispatchToProps: MapDispatchToPropsFunction<CakesViewDispatchProps, any> = dispatch => {
  return {
    loadCakes: () => dispatch(getAllCakes() as any)
  };
};

export const CakesView = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CakesViewComponent)
);
