import { combineReducers } from 'redux';

import appReducer, { AppState } from './app';

export type FullState = {
  app: AppState;
};

export default combineReducers<FullState>({
  app: appReducer
});
