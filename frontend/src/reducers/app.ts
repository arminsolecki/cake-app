import { Reducer } from 'redux';
import { Actions } from '../constants/Actions';
import { ActionTypes } from '../actions';
import { CakeProps } from '../components/Cake/Cake';

export interface AppState {
  cakes: CakeProps[];
  fullCake?: CakeProps;
}

export const initialState: AppState = {
  cakes: []
};

const appReducer: Reducer<AppState> = (state = initialState, action: ActionTypes): AppState => {
  switch (action.type) {
    case Actions.SetAllCakes: {
      return {
        ...state,
        cakes: action.cakes
      };
    }

    case Actions.SetFullCake: {
      return {
        ...state,
        fullCake: action.fullCake
      };
    }
    default:
      return state;
  }
};

export default appReducer;
