import appReducer, { initialState } from './app';
import { Actions } from '../constants/Actions';
import { CakeProps } from '../components/Cake/Cake';
import { createCake } from '../__test-utils__';

describe('app reducer', () => {
  it('returns default state when nothing passed in', () => {
    // Act
    const result = appReducer(undefined as any, {
      type: null
    });

    // Assert
    expect(result).toBeDefined();
  });

  it('returns default state when action is not handled', () => {
    // Act
    const result = appReducer(initialState, {
      type: null
    });

    // Assert
    expect(result).toBe(initialState);
  });

  it('sets all cakes', () => {
    // Arrange
    const cakes: CakeProps[] = [1, 2, 3].map(createCake);

    // Act
    const result = appReducer(initialState, {
      type: Actions.SetAllCakes,
      cakes: cakes
    });

    expect(result.cakes).toBe(cakes);
  });

  it('sets full cake', () => {
    // Arrange
    const cake: any = {};

    // Act
    const result = appReducer(initialState, {
      type: Actions.SetFullCake,
      fullCake: cake
    });

    expect(result.fullCake).toBe(cake);
  });
});
