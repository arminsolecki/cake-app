import * as React from 'react';
import { Link } from 'react-router-dom';
import { Routes } from '../../constants/Routes';
import './Cake.css';
import { getImageUrl } from '../../utils/data';

export interface CakeProps {
  id: number;
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: number;
}

export const Cake: React.StatelessComponent<CakeProps> = ({ id, name, comment, imageUrl, yumFactor }) => {
  return (
    <div className="cake" data-selector="cake">
      <Link to={Routes.ViewCake.replace(':id', id.toString())}>
        <h3 className="cake__title">{name}</h3>
        <img src={getImageUrl(imageUrl)} alt={name} />
      </Link>
    </div>
  );
};
