import * as React from 'react';
import { mount } from 'enzyme';
import { CakeProps } from '../Cake/Cake';
import { CakeList } from './CakeList';
import { MemoryRouter } from 'react-router-dom';

const createCake = (x: number): CakeProps => {
  return {
    id: x,
    name: `Comment ${x}`,
    comment: `Comment ${x}`,
    imageUrl: 'url',
    yumFactor: x
  };
};

describe('<CakeList />', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render list items', () => {
    // Arrange
    const cakes: CakeProps[] = [1, 2, 3, 4].map(createCake);
    const element = (
      <MemoryRouter>
        <CakeList cakes={cakes} />
      </MemoryRouter>
    );

    // Act
    const wrapper = mount(element);
    const renderedCakes = wrapper.find('[data-selector="cake"]');

    // Assert
    expect(renderedCakes).toHaveLength(4);
    cakes.forEach((cake, index) => {
      expect(renderedCakes.at(index).text()).toBe(cake.name);
    });
  });

  it('should render error message when there are no cakes', () => {
    // Arrange
    const cakes: CakeProps[] = [];
    const element = (
      <MemoryRouter>
        <CakeList cakes={cakes} />
      </MemoryRouter>
    );

    // Act
    const wrapper = mount(element);

    // Assert
    expect(wrapper.text()).toBe('No cakes available');
  });
});
