import * as React from 'react';
import { CakeProps, Cake } from '../Cake/Cake';
import './CakeList.css';

export interface CakeListProps {
  cakes: CakeProps[];
}

export const CakeList: React.StatelessComponent<CakeListProps> = ({ cakes }) => {
  let list;
  if (cakes.length) {
    list = cakes.map(props => <Cake {...props} key={props.id} />);
  }

  return <div className="cake-list">{list || 'No cakes available'}</div>;
};
