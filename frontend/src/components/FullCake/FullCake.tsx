import * as React from 'react';
import { getImageUrl } from '../../utils/data';

export interface FullCakeProps {
  id: number;
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: number;
}

export const FullCake: React.StatelessComponent<FullCakeProps> = ({ id, name, comment, imageUrl, yumFactor }) => {
  return (
    <div className="cake">
      <h3 className="cake__title">{name}</h3>
      <div>
        <img src={getImageUrl(imageUrl)} alt={name} />
      </div>
      <div>Comment: {comment}</div>
      <div>Yum Factor: {yumFactor}</div>
    </div>
  );
};
