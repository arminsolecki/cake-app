import * as React from 'react';
import './FormInput.css';

export const FormInput = (props: any) => {
  const { id, label, ...restOfProps } = props;

  return (
    <div className="form-item">
      <label htmlFor={id}>{label}</label>
      <input id={id} {...restOfProps} />
    </div>
  );
};
