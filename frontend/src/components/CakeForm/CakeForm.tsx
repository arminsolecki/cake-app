import * as React from 'react';
import { FormInput } from '../FormInput/FormInput';

export interface CakeFormState {
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: string;
}

export interface CakeFormProps {
  onSubmit: (name: string, comment: string, imageUrl: string, yumFactor: string) => void;
}

export class CakeForm extends React.Component<CakeFormProps, CakeFormState> {
  constructor(props: any) {
    super(props);

    this.state = {
      name: '',
      comment: '',
      imageUrl: '',
      yumFactor: '3'
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: React.ChangeEvent<HTMLElement>) {
    const target: any = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    } as any);
  }

  async handleSubmit(event: React.FormEvent) {
    event.preventDefault();

    const { name, comment, imageUrl, yumFactor } = this.state;
    if (!name || !comment || !imageUrl || !yumFactor) {
      alert('Please fill in all fields');
      return;
    }
    const yum = parseInt(yumFactor, 10);
    if (yum > 5 || yum < 1) {
      alert('Yum factor needs to be between 1 and 5');
      return;
    }

    this.props.onSubmit(name, comment, imageUrl, yumFactor);
  }

  public render() {
    return (
      <div className="form">
        <form onSubmit={this.handleSubmit}>
          <FormInput
            value={this.state.name}
            onChange={this.handleChange}
            id="name"
            name="name"
            label="Name"
            type="text"
          />
          <FormInput
            value={this.state.comment}
            onChange={this.handleChange}
            id="comment"
            name="comment"
            label="Comment"
            type="text"
          />
          <FormInput
            value={this.state.imageUrl}
            onChange={this.handleChange}
            id="imageUrl"
            name="imageUrl"
            label="Image Url"
            type="text"
          />
          <FormInput
            value={this.state.yumFactor}
            onChange={this.handleChange}
            id="yumFactor"
            name="yumFactor"
            label="Yum Factor"
            type="range"
            min="1"
            max="5"
          />
          <button>Creake new cake</button>
        </form>
      </div>
    );
  }
}
