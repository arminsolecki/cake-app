import * as React from 'react';
import { CakeForm } from './CakeForm';
import { mount } from 'enzyme';

describe('<CakeForm />', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render form', () => {
    // Arrange
    const onSubmit = jest.fn();
    const element = <CakeForm onSubmit={onSubmit} />;

    // Act
    const wrapper = mount(element);

    // Assert
    expect(wrapper.find('input')).toHaveLength(4);
  });

  it('should handle submit', () => {
    // Arrange
    const onSubmit = jest.fn();
    const element = <CakeForm onSubmit={onSubmit} />;

    // Act
    const wrapper = mount(element);
    const inputs = wrapper.find('input');

    inputs.at(0).simulate('change', { target: { value: 'Some name', name: 'name' } });
    inputs.at(1).simulate('change', { target: { value: 'Comment', name: 'comment' } });
    inputs.at(2).simulate('change', { target: { value: 'My new value', name: 'imageUrl' } });
    inputs.at(3).simulate('change', { target: { value: '4', name: 'yumFactor' } });

    expect(onSubmit).not.toBeCalled();

    wrapper.update();

    wrapper.find('button').simulate('submit');

    // Assert
    expect(onSubmit).toBeCalledWith('Some name', 'Comment', 'My new value', '4');
  });

  it('should display error when all fields are not filled', () => {
    // Arrange
    const onSubmit = jest.fn();
    const element = <CakeForm onSubmit={onSubmit} />;
    const alertSpy = jest.spyOn(window, 'alert');

    // Act
    const wrapper = mount(element);

    wrapper.find('button').simulate('submit');

    // Assert
    expect(alertSpy).toBeCalledWith('Please fill in all fields');
    expect(onSubmit).not.toBeCalled();
  });

  it('should display error when yum factor is not in range', () => {
    // Arrange
    const onSubmit = jest.fn();
    const element = <CakeForm onSubmit={onSubmit} />;
    const alertSpy = jest.spyOn(window, 'alert');

    // Act
    const wrapper = mount(element);

    const inputs = wrapper.find('input');
    inputs.at(0).simulate('change', { target: { value: 'Some name', name: 'name' } });
    inputs.at(1).simulate('change', { target: { value: 'Comment', name: 'comment' } });
    inputs.at(2).simulate('change', { target: { value: 'My new value', name: 'imageUrl' } });
    inputs.at(3).simulate('change', { target: { value: '7', name: 'yumFactor' } });

    wrapper.update();

    wrapper.find('button').simulate('submit');

    // Assert
    expect(alertSpy).toBeCalledWith('Yum factor needs to be between 1 and 5');
    expect(onSubmit).not.toBeCalled();
  });
});
