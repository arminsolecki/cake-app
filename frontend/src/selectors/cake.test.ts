import { FullState } from '../reducers';
import { createCake } from '../__test-utils__';
import { getAllCakes, getFullCake } from './cake';

describe('cake selector', () => {
  it('should return all cakes', () => {
    // Arrange
    const expectedValue = [1, 2].map(createCake);
    const state: FullState = {
      app: {
        cakes: expectedValue
      }
    };

    // Act
    const result = getAllCakes(state);

    // Assert
    expect(result).toBe(expectedValue);
  });

  it('should return full cake', () => {
    // Arrange
    const expectedValue = createCake(2);
    const state: FullState = {
      app: {
        cakes: [],
        fullCake: expectedValue
      }
    };

    // Act
    const result = getFullCake(state);

    // Assert
    expect(result).toBe(expectedValue);
  });
});
