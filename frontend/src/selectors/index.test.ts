import * as selectors from './index';

describe('selectors', () => {
  it('should have modules defined', () => {
    // Act
    expect(selectors.default.cake).toBeDefined();
  });
});
