import { createSelector } from 'reselect';
import { FullState } from '../reducers/index';

const getAppState = (state: FullState) => state.app;

export const getAllCakes = createSelector([getAppState], appState => appState.cakes);
export const getFullCake = createSelector([getAppState], appState => appState.fullCake);
