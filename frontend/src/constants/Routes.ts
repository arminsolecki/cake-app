export enum Routes {
  Home = '/',
  AddCake = '/add-cake',
  ViewCake = '/cake/:id'
}
