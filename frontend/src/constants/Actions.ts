export enum Actions {
  SetAllCakes = 'SetAllCakes',
  SetFullCake = 'SetFullCake'
}
