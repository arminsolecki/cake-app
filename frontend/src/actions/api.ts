import { ThunkAction } from 'redux-thunk';
import { ApiHelper } from '../utils/api';
import { Actions } from '../constants/Actions';
import { FullState } from '../reducers';
import { Routes } from '../constants/Routes';
import { push } from 'react-router-redux';

export function getAllCakes(): ThunkAction<void, FullState, {}, any> {
  return async (dispatch, getState) => {
    try {
      const cakes = await ApiHelper.getAll();

      dispatch({
        type: Actions.SetAllCakes,
        cakes: cakes
      });
    } catch (error) {
      alert('There was an problem with API');
    }
  };
}

export function getFullCake(id: number): ThunkAction<void, FullState, {}, any> {
  return async (dispatch, getState) => {
    try {
      const cake = await ApiHelper.getById(id);

      dispatch({
        type: Actions.SetFullCake,
        fullCake: cake
      });
    } catch (error) {
      alert('There was an problem with API');
    }
  };
}

export function createCake(
  name: string,
  comment: string,
  imageUrl: string,
  yumFactor: string
): ThunkAction<void, FullState, {}, any> {
  return async (dispatch, getState) => {
    try {
      await ApiHelper.createCake({ name, comment, imageUrl, yumFactor });

      dispatch(push(Routes.Home));
    } catch (error) {
      alert('There was an problem with API');
    }
  };
}
