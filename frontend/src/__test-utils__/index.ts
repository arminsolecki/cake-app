import { CakeProps } from '../components/Cake/Cake';

export const createCake = (x: number): CakeProps => {
  return {
    id: x,
    name: `Comment ${x}`,
    comment: `Comment ${x}`,
    imageUrl: 'url',
    yumFactor: x
  };
};

export const mockResponse = (status: number, statusText: any, response: any) => {
  return new (window as any).Response(response, {
    status: status,
    statusText: statusText
  });
};
