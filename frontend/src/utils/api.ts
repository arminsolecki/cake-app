export class ApiHelper {
  static async getAll() {
    const resp = await fetch('/api/cake');
    const data = await resp.json();

    return data;
  }

  static async getById(id: number) {
    const resp = await fetch('/api/cake/' + id);
    const data = await resp.json();

    return data;
  }

  static async createCake({ name, comment, imageUrl, yumFactor }: any) {
    const resp = await fetch('/api/cake/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name, comment, imageUrl, yumFactor })
    });

    return resp.status;
  }
}
