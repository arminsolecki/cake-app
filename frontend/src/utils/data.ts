import { IMAGE_PATH } from '../constants/Config';

export const getImageUrl = (imageUrl: string) => {
  if (imageUrl && imageUrl.indexOf('http') >= 0) {
    return imageUrl;
  }

  return IMAGE_PATH + imageUrl;
};
