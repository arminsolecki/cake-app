import { IMAGE_PATH } from '../constants/Config';
import { getImageUrl } from './data';

describe('getImageUrl', () => {
  it('should return correct image', () => {
    const testCases = [
      {
        image: 'cake1.jpeg',
        expected: IMAGE_PATH + 'cake1.jpeg'
      },
      {
        image: 'http://test.invalid/cake1.jpeg',
        expected: 'http://test.invalid/cake1.jpeg'
      }
    ];

    testCases.forEach(test => {
      expect(getImageUrl(test.image)).toBe(test.expected);
    });
  });
});
