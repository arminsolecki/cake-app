# Cake App

Application consists of two elements:

- Backend API written in Node/Express
- Front-end SPA written in React/Redux

Both solutions are using typescript.

# Requirements

To run the app following you need to

- Node (I'm using v9.3.0)
- Yarn (I'm using 1.3.2)

# Installation

In order to install all dependencies you need to run

- _yarn install_

in _/backend_ and _/frontend_ folders.

# Running commands

In order run the applications there are following commands that you can use from each of the folders:

- _yarn start_ - starts the application in development mode
- _yarn build_ - builds the application for production

## Back-end

API by default runs on port 4000. You can access the api via following URL

- http://localhost:4000/api/cake/

## Front-end

API by default runs on port 3000. You can access the SPA via following URL

- http://localhost:3000/

The API requests to backend endpoint are PROXIED via package.json setting to avoid CORS

# Unit Testing

Both applications are unit tested. The coverage is not the level that I usually do for production but shows that I'm comfortable with it.

In order to run tests you use

- _yarn test_

# Issues

The provided link to the API (http://ec2-34-243-153-154.eu-west-1.compute.amazonaws.com:5000/api) wasn't working when I was doing the test, hence I had seeded the API DB with some random cakes and added some images into SPA _images_ folder.

# Author

Armin Solecki (armin.solecki@gmail.com)
